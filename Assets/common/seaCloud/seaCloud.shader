﻿/*
Copyright (c) 2019 vrc gitCommitter https://gitlab.com/vrc-gitcommitter/
Released under the MIT license
https://opensource.org/licenses/mit-license.php
*/
Shader "seaCloud"
{
    Properties
    {
        _noiseSource ("_noiseSource", 2D) = "" {}
        [Normal]_noiseSourceNormal ("_noiseSourceNormal", 2D) = "" {}
        _cloudY ("_cloudY", Float) = 0
        _heightDifference ("_heightDifference", Float) = 1
    [Space]
        _topColor ("_topColor", Color) = (0.8, 0.9, 0.9, 1.0)
        _bottomColor ("_bottomColor", Color) = (0.3, 0.4, 0.5, 0.8)
        _topFactor ("_topFactor", Range (0, 3)) = 1
        _topBias ("_topBias", Range (0, 1)) = 0.3
    [Space]
        _fogColor ("_fogColor", Color) = (0.8, 0.9, 1.0, 1.0)
        _fogForce ("_fogForce", Range (0, 1)) = 0.3
    [Space]
        _directionalLightNum ("_directionalLightNum", Int) = 1
        _specularForce ("_specularForce", Range (0, 1)) = 1.0
        _specularLocally ("_specularLocally", Range (1, 40)) = 6
        _directionalLightForce ("_directionalLightForce", Range (0, 1)) = 1
        _dummyLightVector ("_dummyLightVector", Vector) = (0.5, 0.1, 0.5, 0.0)
        _dummyLightSpecularColor ("_dummyLightSpecularColor", Color) = (0.0, 0.0, 0.0, 1.0)
        _dummyLightDiffuseColor ("_dummyLightDiffuseColor", Color) = (0.0, 0.0, 0.0, 1.0)
    [Space]
        _turbulence ("_turbulence", Float) = 0.3
    }
    SubShader
    {
        Tags 
        {
            "Queue"="Transparent"
            "RenderType"="Transparent"
            "LightMode"="Vertex"
        }
        Cull Front

        Pass
        {
            Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "UnityShaderVariables.cginc"
            
            // パラメータ
            uniform sampler2D _noiseSource;
            uniform sampler2D _noiseSourceNormal;
            uniform float4 _topColor;
            uniform float4 _bottomColor;
            uniform float _topFactor;
            uniform float _topBias;
            uniform float _cloudY;
            uniform float _heightDifference;
            uniform float4 _fogColor;
            uniform float _fogForce;
            uniform float _specularForce;
            uniform float _specularLocally;
            uniform float _directionalLightNum;
            uniform float _directionalLightForce;
            uniform float4 _dummyLightSpecularColor;
            uniform float4 _dummyLightVector;
            uniform float4 _dummyLightDiffuseColor;
            uniform float _turbulence;
            
            // ビルトイン
            float4 _LightColor0;

            // 定数


            // ====vertex====
            // セマンティクス
            struct v2f
            {
                float4 vertex : SV_POSITION;
                float4 worldPosition : TEXCOORD0;
            };

            v2f vert (float4 vertex : POSITION)
            {
                v2f o;
                // 必要なワールド位置を引き継ぐ
                float4 worldPosition = mul(unity_ObjectToWorld, vertex);
                o.worldPosition = worldPosition;
                o.vertex = UnityObjectToClipPos(vertex);
                return o;
            }

            // ====fragment====
            // セマンティクス
            struct frag_out
            {
                fixed4 color : SV_TARGET;
                float depth : SV_DEPTH;
            };

            /*
            中央が0のノイズを返す
            */
            float4 createNoise(float2 xz, sampler2D source, float noiseSwing, float scale, float2 speed)
            {
                const float speedOffset = 200;
                float2 uv = xz / scale;
                uv -=  speed * (_Time.x + speedOffset) / scale;
                float4 noise = (tex2D(source, uv) - 0.5) * noiseSwing;
                return noise;
            }

            /*
            複数ノイズを組み合わせて雲を作る
            */
            float4 createCloudNoise(float2 xz, float cameraDistance, sampler2D source, int normal)
            {
                // ----定数----
                // 重ねる数
                const int loopLength = 4;
                // 規定数
                const float swingBase = 0.50;
                const float scaleBase = 10;
                const float2 speedBase = float2(7, 7);
                // 変化量
                const float rotateFactor = UNITY_PI * 13 / 180;
                // 変数
                float scale = scaleBase;
                float2 speed = speedBase;
                float rotate = 0;
                float2x2 rotateMatrix;
                // すべての変化量の冪乗係数
                const float duplicateDistanceInterval = 1.0 / 3;
                // ----距離による導出----
                // 値が極端にならないように抑える
                cameraDistance = clamp(cameraDistance, 0, 400);
                // 距離を分解
                float duplicateFactor = clamp(pow(cameraDistance, 0.5) * duplicateDistanceInterval, 0, 20);
                int loopBase = trunc(duplicateFactor);
                float loopRemainder = frac(duplicateFactor);
                // ----ノイズを重ねて生成する----
                float4 noise = 0.5;
                float swing = pow(0.5, loopLength);
                float swingStrength;
                int loopCount;
                for (int i = 0; i < loopLength; i++)
                {
                    // 合成量を決定
                    swingStrength = 1;
                    if (i == 0)
                    {
                        swingStrength -= loopRemainder;
                    }
                    if (i == loopLength - 1)
                    {
                        swingStrength += loopRemainder - 1;
                    }
                    // 合成量からノイズ振幅を決定
                    swing = (1.0 / loopLength) * swingStrength * pow(1.2, loopCount); //////
                    // 実際のループ数
                    loopCount = i + loopBase;
                    // 各パラメータを決定
                    scale = scaleBase * pow(1.6, loopCount) + 2 * loopCount;
                    speed = speedBase * pow(0.4, loopCount);
                    rotate = loopCount * rotateFactor;
                    rotateMatrix = float2x2
                    (
                        cos(rotate), -sin(rotate), // 軽量化できそう
                        sin(rotate), cos(rotate)
                    );
                    // ノイズを重ねる
                    noise += createNoise(mul(rotateMatrix, xz), source, swing, scale, 1 - mul(-rotateMatrix, speed));
                }
                return clamp(noise, 0, 1);
            }

            /*
            雲のノーマルマップを作成
            */
            float3 createCloudNoise_normal(float2 xz, float cameraDistance, sampler2D source)
            {
                // ノーマルマップとして画像の合成結果を取得
                float4 normalMap = createCloudNoise(xz, cameraDistance, _noiseSourceNormal, 1);
                float3 normal = UnpackNormal(normalMap);
                // 上方向の座標に変換
                return normalize(float3(normal.x, normal.z, normal.y));
            }
            
            /*
            depthの出力
            */
            inline float getDepth(float4 clippos)
            {
                #if defined(SHADER_TARGET_GLSL) || defined(SHADER_API_GLES) || defined(SHADER_API_GLES3)
                    return (clippos.z / clippos.w) * 0.5 + 0.5;
                #else
                    return clippos.z / clippos.w;
                #endif
            }

            /*
            ライト計算
            */
            inline float4 calcLight(float4 cloudColor, float3 normal, float3 lightPos, float4 lightColor, float3 viewDir)
            {
                // ----ディフューズ----
                float3 diffuse = max(-1, dot(normalize(lightPos.xyz), normal));
                cloudColor.rgb += diffuse * _dummyLightDiffuseColor.rgb * cloudColor.a;
                // ----スペキュラ----
                // ライトとカメラの中間ベクトルを作る
                float3 cameraLightHalfDir = normalize(lightPos + viewDir);
                // 法線ベクトルと中間ベクトルからスペキュラーを作成
                float specular = pow(max(0, dot(normal, cameraLightHalfDir)), _specularLocally) * _specularForce;
                // スペキュラー値に合わせてライトの色を足す
                cloudColor.rgb += specular * lightColor.rgb * cloudColor.a;
                return cloudColor;
            }
            /*
            ライト計算（unity_LightPositionの使用）
            */
            inline float4 calcLight_unity(float4 cloudColor, float3 normal, float4 lightPos_unity, float4 lightColor, float3 viewDir)
            {
                // 影響を受けるのはディレクショナルライトのみ
                if (lightPos_unity.w != 0)
                {
                    return cloudColor;
                }
                float3 lightPos = normalize(mul(lightPos_unity, UNITY_MATRIX_IT_MV));
                return calcLight(cloudColor, normal, lightPos, lightColor, viewDir);
            }

            inline float bias(float b, float x)
            {
                return pow(x, log(b) / log(0.5));
            }

            frag_out frag (v2f i)
            {
                // 値の準備
                frag_out o;

                // ----カメラ方向から計算するべき雲のワールド座標を算出----
                // カメラへの視点ベクトル
                float3 viewDir = normalize(UnityWorldSpaceViewDir(i.worldPosition));
                // カメラからの視点ベクトル
                float3 cameraDir = -viewDir;
                // カメラから雲高さまでの倍率を得る
                float cameraToCloudRate = (_cloudY - _WorldSpaceCameraPos.y) / cameraDir.y;
                // 倍率を元に座標計算
                float3 cameraToCloud = cameraDir * cameraToCloudRate;
                float3 originalCloudPos = _WorldSpaceCameraPos + cameraToCloud;
                // y座標はトートロジーを起こすので直接代入
                originalCloudPos.y = _cloudY;

                // ----共通変数----
                float cameraDistance = length(cameraToCloud);

                // ----ノーマルマップから座標をズラすことで雲の形を生成（高いと乱流のようになる）----
                float3 dummyNormalMap = createCloudNoise_normal(originalCloudPos.xz, cameraDistance, _noiseSourceNormal);
                float3 offsetCloudPos = originalCloudPos - dummyNormalMap * _turbulence;

                // ----ワールド座標から雲模様を作成----
                // ノイズを生成
                float noise = createCloudNoise(offsetCloudPos.xz, cameraDistance, _noiseSource, 1).x;
                // バイアス値によって補正をかける
                float topBiasRate = 1 - bias(1 - _topBias, 1 - noise);
                // 色の決定
                float4 cloudColor = topBiasRate * _topColor * _topFactor + (1 - topBiasRate) * _bottomColor;
                // 雲模様から最終的な高さを得る（y値が違うのはnormalのyは他の値によって減らされる相対的な値だから、hightによって取らないといけないため）
                offsetCloudPos.y = _cloudY + noise * _heightDifference;

                // ----光の影響を計算----
                // ノーマルマップを再生成
                float3 normal = createCloudNoise_normal(offsetCloudPos.xz, cameraDistance, _noiseSourceNormal);
                // ディレクショナルライトによる光量の計算
                float4 lightColor;
                for (int i = 0; i < _directionalLightNum; i++)
                {
                    lightColor = unity_LightColor[i] * _directionalLightForce;
                    cloudColor = calcLight_unity(cloudColor, normal, unity_LightPosition[i], lightColor, viewDir);
                }
                // 偽のライトによる光量計算
                cloudColor = calcLight(cloudColor, normal, normalize(_dummyLightVector.xyz), _dummyLightSpecularColor, viewDir);

                // ----フォグの影響を計算----
                // 近距離度合いを算出（直上直下が1、無限遠が0）
                float verticality = abs(atan2(cameraToCloud.y, length(cameraToCloud.xz)));
                // フォグの強さ(遠方からforceの量までで0になる)
                float fogRate = 1 - clamp(verticality / _fogForce, 0, 1);
                cloudColor = fogRate * _fogColor + (1 - fogRate) * cloudColor;
                
                // ----depthを計算----
                // 視点viewベクトル
                float4 vpPos = mul(UNITY_MATRIX_VP, float4(offsetCloudPos, 1.0));
                float depth = getDepth(vpPos);
                
                // ----後処理----
                // カメラ倍率がマイナス（カメラの後方に雲がある）の場合ピクセル描画を破棄
                clip(cameraToCloudRate);
                // 値の出力
                o.color = cloudColor;
                o.depth = depth;
                
                // ----デバッグ----
                // const float duplicateDistanceMin = 10;
                // const float duplicateDistanceInterval = 1.0 / 20;
                // float duplicateFactor = log(max(duplicateDistanceMin, cameraDistance) * duplicateDistanceInterval);
                // float loopBase = floor(duplicateFactor);
                // loopBase = 1;
                // float3 debug = noise;

                // o.color = fixed4(debug, 1);
                // debug3.g = cameraToCloudRate;
                // o.color = fixed4(debug.r, 0.5, 0.5, 1);
                // o.color = fixed4(0.5, debug.g, 0.5, 1);
                // o.color = fixed4(0.5, 0.5, debug.b, 1);
                // o.color = fixed4(debug.x, debug.y, debug.z, 1);
                // float debug = 1 - frac(log(max(10, length(cameraToCloud)) * 0.005));
                // frac(log(max(10, length(cameraToCloud)) * 0.005)); // 濃くなっていくので値が大きい方
                // debug = tex2D(_noiseSource, cloudPos.xz);
                // o.color = fixed4(debug.r * 0.5 + 0.5, 0.5, 0.5, 1);
                // o.color = fixed4(0.5, 0.5, 0.5, 1);
                return o;
            }
            ENDCG
        }
    }
}
